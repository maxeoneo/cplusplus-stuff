#include <iostream>
#include <vector>
#include <gtest/gtest.h>

using namespace std;

vector<int> calculatePrimeFactors(const int number)
{
  int rest = number;
  vector<int> result;

  int candidate = 2;
  while (rest > 1)
  {
    while (rest % candidate == 0)
    {
      result.push_back(candidate);
      rest = rest / candidate;
    }
    ++candidate;
  }

  return result;
}

// Unittest *************************
void compareVectors(const vector<int> first, const vector<int> second)
{
  ASSERT_EQ(first.size(), second.size());

  for (int i = 0; i < first.size(); ++i)
  {
    EXPECT_EQ(first.at(i),second.at(i));
  }
}

int main()
{
  map<int, vector<int>> testCases {
    {0, {} }
    , {1, {} }
    , {2, {2} }
    , {3, {3} }
    , {4, {2, 2} }
    , {5, {5} }
    , {6, {2, 3} }
    , {7, {7} }
    , {8, {2, 2, 2} }
    , {9, {3,3} }
    , {10,{2,5} }
    , {11, {11} }
    , {12, {2, 2, 3} }
    , {13, {13} }
    , {14, {2, 7} }
    , {15, {3, 5} }
    , {16, {2, 2, 2, 2} }
    , {17, {17} }
    , {18, {2, 3, 3} }
    , {19, {19} }
    , {20, {2, 2, 5} }
  };

  map<int, vector<int>>::iterator it;
  for (it = testCases.begin(); it != testCases.end(); it++)
  {
    compareVectors(it->second, calculatePrimeFactors(it->first));
  }

  return 0;
}