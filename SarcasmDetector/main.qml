import QtQuick 2.7
import QtQuick.Window 2.2
import QtMultimedia 5.6

Window {
  visible: true
  width: 1080
  height: 1920
  title: qsTr("Sarcasm Detector")

  property int percentOfSarcasm: 0

  onPercentOfSarcasmChanged: {
    leftDisplay.percentFilled = percentOfSarcasm;
    rightDisplay.percentFilled = percentOfSarcasm;
  }

  SarcasmDisplay {
    id: leftDisplay
    anchors.left: parent.left
    anchors.right: parent.horizontalCenter
    anchors.top: parent.top
    anchors.bottom: percentageText.top

    anchors.topMargin: parent.width / 5
    anchors.leftMargin: anchors.topMargin
    anchors.rightMargin: 10
    anchors.bottomMargin: anchors.topMargin

    percentFilled: percentOfSarcasm
  }

  SarcasmDisplay {
    id: rightDisplay
    anchors.left: parent.horizontalCenter
    anchors.right: parent.right
    anchors.top: parent.top
    anchors.bottom: percentageText.top

    anchors.topMargin: parent.width / 5
    anchors.leftMargin: 10
    anchors.rightMargin: anchors.topMargin
    anchors.bottomMargin: anchors.topMargin

    percentFilled: percentOfSarcasm
  }

  Text {
    id: percentageText
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 50

    text: percentOfSarcasm + "% Sarkasmus"
  }

  MediaPlayer {
    id: playMusic
    source: "beep-07.mp3"
  }

  Timer {
    id: beepTimer
    running: true
    repeat: true
    interval: playMusic.duration + 50 * (100 - percentOfSarcasm)
    onTriggered: {
      console.log("PLAY")
      playMusic.play()
      console.log("ERROR: " + playMusic.errorString)
    }
  }

  MouseArea {
    anchors.fill: parent
    onClicked: {
      var smallHeight = height * 0.8
      var smallMouseY = mouseY - (height * 0.1)
      if (mouseY < height * 0.1)
      {
        percentOfSarcasm = 100;
      }
      else if (mouseY > height * 0.9)
      {
        percentOfSarcasm = 0
      }
      else
      {
        percentOfSarcasm = (smallHeight - smallMouseY) / smallHeight * 100;
      }

    }
  }
}
