import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle {
  id: displayRoot

  property int percentFilled: 0

  border.width: 2
  border.color: "black"

  LinearGradient {
    anchors.fill: parent
    anchors.margins: 2
    start: Qt.point(0, 0)
    end: Qt.point(0, displayRoot.height)
    gradient: Gradient {
      GradientStop { position: 0.0; color: "red" }
      GradientStop { position: 0.2; color: "orange" }
      GradientStop { position: 1.0; color: "green" }
    }

    Rectangle {
      id: percentUnfiller
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.top: parent.top
      height: parent.height - (parent.height / 100 * percentFilled)

      // This is the behavior, and it applies a NumberAnimation to any attempt to set the x property
      Behavior on height {

        NumberAnimation {
          //This specifies how long the animation takes
          duration: 1000
          //This selects an easing curve to interpolate with, the default is Easing.Linear
          easing.type: Easing.OutBounce
        }
      }

      color: "black"
    }

    Repeater {
      anchors.fill: parent
      model: 9

      delegate: Rectangle {
        width: displayRoot.width - 4
        height: 2
        y: (index + 1) * displayRoot.height / 10
        color: "black"
      }
    }
  }

  Timer {
    interval: 500
    running: true
    repeat: true
    onTriggered: {
      if (percentFilled === 0)
      {
        percentFilled = 1;
      }
      else if (percentFilled === 100)
      {
        percentFilled = 99;
      }
      else
      {
        if (Math.random() > 0.5)
        {
          percentFilled += 1;
        }
        else
        {
          percentFilled -= 1;
        }
      }
    }
  }


}
